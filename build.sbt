scalaVersion := "2.13.1"

name := "new-scheme"
organization := "cn.librechair"
version := "0"
libraryDependencies ++= Seq(
  compilerPlugin(
    "com.github.ghik" % "silencer-plugin" % "1.4.4" cross CrossVersion.full
  ),
  "com.github.ghik" % "silencer-lib" % "1.4.4" % Provided cross CrossVersion.full
)
