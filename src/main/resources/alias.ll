(assign '/ quotient)
(assign '+ plus)
(assign '- minus)
(assign '* product)
(assign '% mod)
(assign '= eq?)
(assign '< less-than?)
(assign '> more-than?)

(assign '𝌌 map-car)
(assign '𝌇 map-cdr)
(assign '𝌓 fold)
(assign '𝌖 fold-right)

(assign 'xcons
	(fn (d a)
	    (cons a d)))

(assign '| or)
(assign '\& and)
(assign '<< shl)
(assign '>> shr)
(assign '^ xor)

(assign 'qq quasiquote)
(assign 'uq unquote)

(defsyntax-alias q quote)
(defsyntax-alias progn do)
(assign 'qu q)

(assign 'logor 'or)
(assign 'logand 'and)
