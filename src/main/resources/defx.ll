;; defx.ll -- def* macros
;; GPL 3.0 or later

(assign 'defn
	(syntax-item (fname fargs _)
		     (assign 'fname
			     (fn fargs _))))

(assign 'defn!
	(syntax-item (fname fargs _)
		     (assign! 'fname
			      (fn fargs _))))

(assign 'defn-dynamic
	(syntax-item (fname fargs _)
		     (assign 'fname
			     (lambda fargs _))))

(assign 'defn-dynamic!
	(syntax-item (fname fargs _)
		     (assign! 'fname
			      (lambda fargs _))))

(assign 'defun defn)

(assign 'defmacro
	(syntax-item (fname fargs _)
		     (assign 'fname
			     (syntax-item fargs _))))  

(assign 'defm defmacro)

(assign 'define
	(syntax-item ((arg (name 0 1) (args 1)) _)
		     (assign (car 'name)
			     (fn args _))))
(assign 'define!
	(syntax-item ((arg (name 0 1) (args 1)) _)
		     (assign! (car 'name)
			      (fn args _))))

(defmacro def (symb expr)
  (assign 'symb expr))

(defmacro def! (symb expr)
  (assign! 'symb expr))
