;; compose.lisp -- `compose' & co
;; GPL-3.0-or-later

(assign 'compose
	(fn (fns--)
	    (fn (args--)
		(let ((compose-loop
		       (fn (funcs lv)
			   (if (nil? funcs)
			       (car lv)
	  		       (compose-loop (cdr funcs)
			            	     (list ((car funcs)
						    #lv)))))))
		     (compose-loop (reverse fns--)
				   args--)))))
