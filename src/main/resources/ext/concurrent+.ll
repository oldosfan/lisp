;; concurrent+.lisp -- extensions to concurrent/*

(require 'ext/lists)

(define (concurrent+/future executor fun)
    (let ((future (list nil)))
      (concurrent/post executor
		       (fn ()
			   (list-set future
				     0
				     (fun))))
      future))

(assign 'concurrent+/future->value car)
