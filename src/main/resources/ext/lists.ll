;; lists.lisp -- Extensions to common list-processing utilities
;; SPDX-License-identifier: GPL-3.0-or-later

(assign 'length+
	(fn (list)
	    (if (nil? (circular-list? list))
		(length list))))
(assign 'fold-with-seed
	(fn (lis seed operator)
	    (if (nil? lis)
		seed
	        (fold-with-seed (cdr lis)
				(operator seed (car lis))
				operator))))

(defun dotted-list? (l)
  (if (cons? (cdr l))
      (dotted-list? (cdr l))
      (and? (cdr l) (nil? (cons? (cdr l))))))

(defmacro dolist (symb exp _)
  (let ((l exp))
    (if l
	(let ((symb (car l)))
	  (if (cdr l)
	      (do _
		  (dolist symb (cdr l)))
	      _)))))

(assign 'zip
	(fn (lists--)
	    (let ((zip-internal
		   (fn (lists acc)
		       (if (filter lists nil?)
			   acc
			   (zip-internal (map-car lists cdr)
					 (+ acc (map-car lists car)))))))
	         (zip-internal lists-- nil))))

(assign 'map-car-with-index
	(fn (mkl oper)
	    (let ((-map-car (fn (mkl oper accuml index)
				(if (nil? mkl)
				    nil
				    (if (nil? (cdr mkl))
					(+ accuml (oper (car mkl) index))
				        (-map-car (cdr mkl)
						  oper
						  (+ accuml (oper (car mkl) index))
						  (+ index 1)))))))
	         (-map-car mkl oper nil 0))))


(assign 'unzip
	(fn (zipped)
	    (let ((unzip-internal
		   (fn (zipped accumulator)
		       (if (nil? zipped)
			   accumulator
			   (unzip-internal (cdr zipped)
					   (map-car-with-index accumulator
							       (fn (object index)
								   (+ object (list-get (car zipped)
										       index)))))))))
	         (unzip-internal zipped (repeat nil
						(length (car zipped)))))))

(assign 'list-set
	(fn (list index value)
	    (if (zero? index)
		(set-car! list value)
	        (list-set (- index 1)
			  value))))
(assign 'first-car
	(fn (lis kns)
	    (if lis
		(if (kns (car lis))
		    (car lis)
      		    (first-car (cdr lis) kns)))))
(assign 'first-cdr
	(fn (lis kns)
	    (if lis
		(if (kns lis)
		    lis
		    (first-cdr (cdr lis) kns)))))
(assign 'last
	(fn (lis)
	    (car (last-pair lis))))

(assign 'split-multiple-indexes
	(fn (lis pt--)
	    (let ((lp (fn (lis pt index ac1 ac2)
			  (if (nil? lis)
			      (+ ac2 ac1)
			      (if (contains? pt index)
				  (lp (cdr lis)
				      (filter pt
					      (fn (nil? (eq? o index))))
				      (+ index 1)
				      (list (car lis))
				      (+ ac2 ac1))
			 	  (lp (cdr lis)
				      pt
				      (+ index 1)
				      (+ ac1 (car lis))
				      ac2))))))
	         (lp lis pt-- 0 nil nil))))  
				    
(assign 'partition
	(fn (lis pred)
	    (let ((partition
		   (fn (lis pred ac1 ac2)
		       (if (nil? lis)
			   (list ac1 ac2)
			   (if (pred (car lis))
			       (partition (cdr lis)
					  pred
					  (+ ac1 (car lis))
					  ac2)
			       (partition (cdr lis)
					  pred
					  ac1
					  (+ ac2 (car lis))))))))
   	         (partition lis pred nil nil))))
(assign 'memq
	(fn (symb sl)
	    (if sl
		(if (eq? (car sl) symb)
		    sl
		    (memq symb
			  (cdr sl))))))

(assign 'member
	(fn (symb eq? sl)
	    (if sl
		(if (eq? (car sl) symb)
		    (car sl)
		    (member symb eq? (cdr sl))))))

(assign 'alist-cons
	(fn (key datum alist)
	    (cons (cons key datum)
		  alist)))

(assign 'alist-rm
	(fn (alist key)
	    (filter alist
		    (fn (nil? (eq? (car o) key))))))

(assign 'alist-create
	(fn (key datum)
	    (alist-cons key datum nil)))

(assign 'alist-get
	(fn (alist key)
	    (when alist
	      (if (eq? (caar alist) key)
		  (cdar alist)
		  (alist-get (cdr alist) key)))))

(assign 'delete
	(fn (lst it)
	    (let ((delete
		   (fn (lst it ac)
		       (if lst
			   (if (nil? (eq? it (car lst)))
			       (delete (cdr lst)
				       it
				       (+ ac (car lst)))
	   		       (delete (cdr lst)
				       it
				       ac))
			   ac))))
	         (delete lst it nil))))
	    
(assign 'assq
	(fn (key alist)
	    (first alist
		   (fn (eq? (car o) key)))))

(assign 'first
	(fn (lis cmp)
	    (if lis
		(if (cmp (car lis))
		    (car lis)
		    (first (cdr lis) cmp)))))
(assign 'iota
	(fn (r /optional b c)
	    (let ((iota-real
		   (fn (r b c)
		       (if (eq? r 1)
			   b
			   (iota-real (- r 1)
				      (if (number? b)
					  (+ (list b) c)
					  (+ b (+ (last b) c)))
				      c)))))
	      (iota-real r 
			 (reqnil b 0)
			 (reqnil c 1)))))

(assign 'create-alist
	(fn (keys values)
	    (let ((create-alist
		   (fn (keys values accumulator)
		       (if (nil? keys)
			   accumulator
			   (create-alist (cdr keys)
					 (cdr values)
					 (+ accumulator
					    (cons (car keys)
						  (car values))))))))
	         (create-alist keys values nil))))

(assign 'delete-duplicates
	(fn (list)
	    (let ((duplicates
		   (fn (list acc)
		       (if (nil? list)
			   acc
			   (if (contains? acc (car list))
			       (duplicates (cdr list) acc)
			       (duplicates (cdr list)
					   (+ acc (car list))))))))
	         (duplicates list nil))))

(assign 'every?
	(fn (lis prd)
	    (let ((every?
		   (fn (lis prd ac)
		       (if (nil? lis)
			   ac
			   (if ac
			       (every? (cdr lis)
				       prd
				       (prd (car lis))))))))
	         (every? lis prd atom))))

(assign 'unfold+
	(fn (p f g seed tail-gen)
	    (if (p seed)
		(tail-gen seed)
                (cons (f seed)
                      (unfold+ p f g (g seed))))))
(assign 'unfold
	(fn (p f g seed)
	    (unfold+ p f g seed
		     (fn nil))))

(defn copy-pair (pair)
  (cons (car pair) (cdr pair)))

(defn copy-list (lis)
  (let ((copy-list
	 (fn (lis accumulator)
	     (if (nil? lis)
		 accumulator
	         (copy-list (cdr lis)
			    (+ accumulator (car lis)))))))
       (copy-list lis nil)))

(defun intersection (list1 list2)
  (defn! intersection (list1 list2 acc)
    (if list1
      (if (contains? list2 (car list1))
	  (intersection (cdr list1)
			list2
			(+ acc (car list1)))
	  (intersection (cdr list1)
			list2
			acc))
      acc))
  (intersection list1 list2 nil))

(defun dotted-list (el--)
  (if (eq? (length+ el--) 2)
      (cons (car el--) (cadr el--))
      (do (defun dotted-list-real (el /optional m)
	    (if (cddr el)
		(dotted-list-real (cdr el)
			      (+ m (car el)))
		(do
		 (set-cdr! (last-pair m)
			   (cons (car el)
				 (cadr el)))
		 m)))
	  (dotted-list-real el--))))


(defun interleave (l e /optional accuml)
  (if (nil? l)
      (append-list accuml e)
      (if (nil? e)
	  (append-list accuml l)
	  (interleave (cdr l)
		      (cdr e)
		      (+ (+ accuml (car l))
			 (car e))))))
