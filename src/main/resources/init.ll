;; init.lisp -- Standard Library
;; SPDX-License-Identifier: GPL-3.0-or-later

(load-file "primv.ll")

(defmacro unless (pred _)
  (if (nil? pred)
      (do _)))

(defmacro when (pred _)
  (if pred (do _)))

(defmacro dotimes (times _)
  (unless (zero? times)
    _
    (dotimes (- times 1) _)))

(defmacro while (pred _)
  (when pred
    _
    (while pred _)))

(defmacro forever (_)
  _
  (forever _))

(defmacro whilenot (pred _)
  (while (nil? pred) _))

(defmacro dorev (_)
  (apply do (reverse '(_))))

(defmacro prog-1 (first _)
  (let ((r first)) _ r))

(defmacro prog-2 (first second _)
  (let ((r first)
	(r2 second))
    _
    r2))

(defmacro prog-3 (first second third _)
  (let ((r first)
	(r2 second)
	(r3 third))
    _
    r3))

(defmacro prog-5 (first second third fourth _)
  first
  second
  third
  (let ((r fourth))
    _
    r))

(defmacro prog-6 (f s t fu fv _)
  f
  s
  t
  fu
  (let ((r fv))
    _
    r))

(assign 'assign-global!
	(fn (symb item)
	    (assign-ref! (parent-environment-ref)
			 symb item)))
(assign 'unpack
	(fn (error "Unpack not expected here")))

(assign 'expand
	(fn (error "Expand not expected here")))

(assign 'require
	(fn (l) (if (nil? (contains? require/imported l))
		    (do (load-file (plus (symbol->string l) ".ll"))
			(assign-global! 'require/imported (plus require/imported l))))))

(assign 'ceil
	(fn (number)
	    (- 0 (floor (- 0 number)))))

(assign 'round
	(fn (number)
	    (if (or? (> (mod number 1) 0.5)
		     (eq? (mod number 1) 0.5))
		(+ number (- 1 (mod number 1)))
      	        (- number (mod number 1)))))

(assign 'defined-natively?
	(fn (procedure)
	    (nil? (procedure->definition procedure))))

(assign 'sign
	(fn (number)
	    (if (> number 0)
		1
	        (if (< number 0)
		    -1
		    0))))
(assign 'zero?
	(fn (arg)
	    (eq? arg 0)))

(assign 'even?
	(fn (arg)
	    (eq? (mod arg 2) 0)))
(assign 'odd?
	(fn (arg)
	    (nil? (even arg))))

(assign 'for-each
        (fn (list cb)
	    (if (nil? (cdr list))
		(cb (car list))
	        (do (cb (car list))
		    (for-each (cdr list) cb)))))
(assign 'list
	(fn (objects--)
	    objects--))

(assign 'length
	(fn (object)
	    (let ((list-length
		   (fn (lst accumulator)
		       (if (nil? (cdr lst))
			   (+ accumulator 1)
			   (list-length (cdr lst)
					(+ accumulator 1)))))
		  (str-length (fn (str) (list-length (string-split str) 0))))
	         (if (string? object)
		     (str-length object)
		     (list-length object 0)))))

(assign 'filter
	(fn (lis prd)
	    (let ((filter-internal
		   (fn (lis prd ac)
		       (if (nil? lis)
			   ac
			   (filter-internal (cdr lis)
					    prd
					    (if (prd (car lis))
	 					(+ ac (car lis))
	   				        ac))))))
	         (filter-internal lis prd nil))))

(assign 'filter-cdr
	(fn (lis prd)
	    (let ((filter-cdr-internal
		   (fn (lis prd ac)
		       (if (nil? (cdr lis))
			   ac
			   (filter-cdr-internal (cdr lis)
						prd
						(if (prd (cdr lis))
						    (+ ac (cdr lis))
						    ac))))))
	         (filter-cdr-internal lis prd nil))))
(assign 'list-get
	(fn (list index)
	    (if (eq? index 0)
		(car list)
	        (list-get
	            (cdr list)
		    (minus index 1)))))

(assign 'reduce
	(fn (lst call)
	    (let ((-reduce (fn (lst call accumulator)
			       (if (nil? (cdr lst))
				   (call accumulator (car lst))
				   (-reduce (cdr lst)
					    call
					    (call accumulator
						  (car lst)))))))
	      (if (nil? lst)
		  nil
		  (if (nil? (cdr lst))
		      (car lst)
		      (if (nil? (cddr lst))
			  (call (car lst)
				(cadr lst))
		          (-reduce (cddr lst)
				   call
				   (call (car lst)
					 (cadr lst)))))))))

(assign 'fold reduce)

(assign 'caar
	(fn (pair)
	    (car (car pair))))
(assign 'cadr
	(fn (pair)
	    (car (cdr pair))))
(assign 'cdar
	(fn (pair)
	    (cdr (car pair))))
(assign 'cddr
	(fn (pair)
	    (cdr (cdr pair))))
(assign 'caaar
	(fn (pair)
	    (car (car (car pair)))))
(assign 'cdaar
	(fn (pair)
	    (cdr (car (car pair)))))
(assign 'cddar
	(fn (pair)
	    (cdr (cdr (car pair)))))
(assign 'cdddr
	(fn (pair)
	    (cdr (cdr (cdr pair)))))
(assign 'caadr
	(fn (pair)
	    (car (car (cdr pair)))))
(assign 'caddr
	(fn (pair)
	    (car (cdr (cdr pair)))))
(assign 'cadar
	(fn (pair)
	    (car (cdr (car pair)))))
(assign 'cdadr
	(fn (pair)
	    (cdr (car (cdr pair)))))
(assign 'caaaar
	(fn (pair)
	    (car (car (car (car pair))))))
(assign 'caaadr
	(fn (pair)
	    (car (car (car (cdr pair))))))
(assign 'caaddr
	(fn (pair)
	    (car (car (cdr (cdr pair))))))
(assign 'cadddr
	(fn (pair)
	    (car (cdr (cdr (cdr pair))))))
(assign 'cddddr
	(fn (pair)
	    (cdr (cdr (cdr (cdr pair))))))
(assign 'cdaddr
	(fn (pair)
	    (cdr (car (cdr (cdr pair))))))
(assign 'cdaaar
	(fn (pair)
	    (cdr (car (car (car pair))))))
(assign 'cddaar
	(fn (pair)
	    (cdr (cdr (car (car pair))))))
(assign 'cdddar
	(fn (pair)
	    (cdr (cdr (cdr (car pair))))))
(assign 'caddar
	(fn (pair)
	    (car (cdr (cdr (car pair))))))
(assign 'cadaar
	(fn (pair)
	    (car (cdr (car (car pair))))))

(assign 'cddadr
	(fn (pair)
	    (cdr (cdr (car (cdr pair))))))
(assign 'cdadar
	(fn (pair)
	    (cdr (car (cdr (car pair))))))
(assign 'cdaadr
	(fn (pair)
	    (cdr (cdr (car (cdr pair))))))

(assign 'string-slice
	(fn (str start end)
	    (reduce (take (after (string-split str)
				 start)
			  (- end start))
		    (fn (a z) (+ (to-string a)
				 (symbol->string z))))))

(assign 'contains?
	(fn (src obj)
	    (let ((containsp
		   (fn (src obj sto)
		       (if (or? (nil? src) sto)
			   sto
			   (if (eq? (car src) obj)
			       atom
			       (containsp (cdr src) obj sto)))))) 
                 (containsp src obj nil))))

(assign 'member?
	(fn (src predicate)
	    (if (nil? src)
		nil
	        (if (predicate (car src))
		    (car src)
		    (member? (cdr src) predicate)))))

(assign 'append-list
	(fn (list list2)
	    (if list
		(if list2
		    (if (cdr list2)
			(append-list (+ list (car list2))
				     (cdr list2))
		        (+ list (car list2)))
		    list)
	        list2)))

(assign 'map-car
	(fn (mkl oper)
	    (let ((-map-car (fn (mkl oper accuml)
				(if (nil? mkl)
				    nil
				    (if (nil? (cdr mkl))
					(+ accuml (oper (car mkl)))
				        (-map-car (cdr mkl)
						  oper
						  (+ accuml (oper (car mkl)))))))))
      	         (-map-car mkl oper nil))))

(assign 'map-cdr
	(fn (list2 oper)
	    (assign '-map-cdr
		    (fn (list2 oper accumulator)
			(if (nil? list2)
			    (+ accumulator (oper nil))
			    (-map-cdr (cdr list2)
				      oper
				      (+ accumulator
					 (oper list2))))))
	    (-map-cdr list2
		      oper
		      '())))

(assign 'after
        (fn (list index)
	    (if (eq? index 0)
		list
      	        (after (cdr list) (- index 1)))))

(assign 'take
	(fn (lst amt)
	    (let ((-take
		   (fn (lst ac amt)
		       (if (zero? amt)
			   ac
			   (-take (cdr lst)
				  (+ ac (cadr lst))
				  (- amt 1))))))
	         (if (nil? (zero? amt))
		     (if (and? (nil? (circular-list? lst))
			       (< (length lst) (+ amt 1)))
			 lst
		         (append-list (list (car lst))
				      (-take lst nil (- amt 1))))))))
(assign 'repeat
	(fn (item amt)
	    (let ((-repeat
		   (fn (object amt acc)
		       (if (zero? amt)
			   acc
			   (-repeat object
			            (- amt 1)
				    (+ acc object))))))
           	 (-repeat item amt nil))))

(assign 'take-right
	(fn (list amt)
	    (reverse (take (reverse list) amt))))

(assign 'drop
	(fn (list amt)
	    (if (zero? amt)
		list
	        (drop (cdr list)
		      (- amt 1)))))
(assign 'drop-right
	(fn (list amt)
	    (take list
		  (- (length list) amt))))

(assign 'fold-right
	(fn (src oper)
	    (reduce (reverse src) oper)))

(assign 'string->number...
	(fn (str)
	    (map-car (string-split str)
		     (fn (symb)
			 (char->number symb)))))

(assign 'split-at
	(fn (lst index)
	    (list (take lst index)
		  (after lst index))))
			

(assign 'reverse
	(fn (list)
	    (let ((rev-internal
		   (fn (to-reverse accumulator)
		       (if (nil? to-reverse)
			   accumulator
			   (rev-internal (cdr to-reverse)
					 (cons (car to-reverse)
					       accumulator))))))
	         (if (circular-list? list)
		     (circular-list &(rev-internal (slice-circular-list list)
						   nil))
		     (rev-internal list nil)))))
(assign 'to-string
	(fn (obj)
	    (if (string? obj)
		obj
                (+ "" obj))))

(assign 'circular-list
	(fn (things--)
	    (set-cdr! (last-pair things--) things--)
	    things--))

(assign 'last-pair
	(fn (list)
	    (if (nil? (cdr list))
		list
	        (last-pair (cdr list)))))
	       
(assign 'map-get
	(fn (map key)
	    (if map
		(if (eq? (caar map) key)
		    (cadar map)
		    (map-get (cdr map) key)))))

(assign 'proper-list?
	(fn (pair)
	    (if (nil? (circular-list? pair))
		(if (nil? (cons? pair))
		    nil
		    (if (nil? (cdr pair))
			atom
		        (proper-list? (cdr pair)))))))


(require 'alias)
(require 'mathal)

(defmacro check-type (type val)
  (unless ((eval (string->symbol (+ (symbol->string 'type) "?"))
		 (environment))
	   val)
    (error (+ "Wrong type for "
	      (symbol->string 'val)))))
	      
