;; io.ll -- I/O operations
;; GPL 3.0 or later

(require 'ext/lists)
(require 'jvm/reflect)

(defmacro make-port (read write)
  (list
   (cons '^read read)
   (cons '^write write)))

(defun port-read (r)
  ((alist-get r 'read)))

(defun port-write (r c)
  ((alist-get r 'write) c))

(defun file-open (path /optional write ovwrt)
  (let ((file
	 (rf:invoke-method
	  (rf:get-constructor
	   (rf:get-class "java.io.File")
	   (rf:get-class "java.lang.String"))
	  nil
	  (rf:to-java path)))
	(is
	 (rf:invoke-method
	  (rf:get-constructor (rf:get-class "java.io.FileInputStream")
			      (rf:get-class "java.io.File"))
	  nil
	  file))
	(os
	 (when write
	   (rf:invoke-method
	    (rf:get-constructor (rf:get-class "java.io.FileOutputStream")
				(rf:get-class "java.io.File")
				(rf:get-pclass 'boolean))
	    nil
	    file
	    (rf:to-java (nil? ovwrt) 'boolean)))))
	(make-port
	 (fn ()
	     (rf:invoke-method (rf:get-method (rf:get-class "java.io.InputStream")
					      "read")
			       is))
	  (when write
	    (fn (i)
		(rf:invoke-method (rf:get-method (rf:get-class "java.io.OutputStream")
						 "write"
						 (rf/class-of
						  (rf:array-of-one (rf:to-java 1))))
				  (rf:array-of-one (rf:to-java i 'byte))))))))
