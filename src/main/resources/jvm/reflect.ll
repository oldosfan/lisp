;; reflect.ll -- reflection utilities

(attach-reflect-module)

(defn rf/wrap-method
  (method)
  (fn (object args--)
      (rf:invoke-method method
			object
			&(map-car args--
				  (fn (if (jobject? o)
					  o
				        (rf:to-java o)))))))
(defn rf/class-of
  (object)
  ((rf/wrap-method (rf:get-method (rf:get-class "java.lang.Object")
				  "getClass"))
   (rf:to-java object)))

(defn rf/invoke-method
  (object name args--)
  (rf:invoke-method (rf:get-method (rf/class-of object)
				   name
				   (map-car args--
					    (fn (rf/class-of o))))
		    object
		    &args--))

(defn rf/get-field (object name)
  (rf:get-field (rf:find-field (rf/class-of object)
			       name)
		object))
