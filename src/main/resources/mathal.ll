;; mathal.ll -- 1+, -1, /=, et cetera

(defmacro 1+ (x)
  (+ x 1))
(defmacro 1- (x)
  (- 1 x))
(defmacro 2+ (x)
  (+ x 2))
(defmacro 2- (x)
  (- 2 x))

(define (0- x) (- x 0))

(define (/= a b)
    (not? (eq? a b)))
