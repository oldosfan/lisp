;; primv.ll -- primitive constructs

(load-file "defx.ll")

(assign 'do
	(syntax-item (_)
		     _))
(assign 'quasiquote
	(syntax-item (statement)
		     (let ((st 'statement))
		       (if (cons? st)
			   (if (eq? (car st) 'unquote)
			       (eval (cadr st) nil)
			       (map-car st
					(fn (if (cons? o)
						(if (eq? (car o) 'unquote)
						    (eval (cadr o) nil)
						    (apply quasiquote (list o)))
						o))))
			   st))))
(assign 'lcons
	(syntax-item (statement)
		     (cons #'statement)))
(assign 'lxcons
	(syntax-item (statement)
		     (xcons #'statement)))

(defmacro defsyntax-alias (key element)
  (assign 'key (syntax-item (^_)
			    (element ^_))))

(defun reqnil (v l)
  (if v v l))
