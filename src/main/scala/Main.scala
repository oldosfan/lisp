import cn.librechair.r4rs.tokenizer.Tokenizer
import java.io.InputStreamReader
import cn.librechair.r4rs.tokenizer.Postprocessor
import cn.librechair.r4rs.exceptions._
import cn.librechair.r4rs.interpreter.types._
import cn.librechair.r4rs.interpreter._
import java.io.File
import java.io.FileInputStream
import java.io.EOFException

object Main {
  val tokenizer     = new Tokenizer(new InputStreamReader(System.in))
  val postprocessor = new Postprocessor(tokenizer)
  var repl          = 0
  
  def main(args: Array[String]): Unit = {
    try {
      Mrt.loadFile(getClass().getClassLoader().getResourceAsStream("init.ll"))
    } catch {
      case SyntaxException(err) => println(s"Syntax error: $err")
      case EvaluationException(err, symb, _, stack) =>
        println(
          s"Error evaluating $symb: ${err.take(1800)} ${if (err.length > 1800) "..."
          else ""}\n STACK TRACE: \n${RT.fancyFormatStack(stack)}"
        )

      case e: EOFException => println("EOF"); System.exit(0)
      case e: Throwable    => e.printStackTrace()
    }
    println(
      "This is the list processor. You are communicating with the command interpreter."
    )
    while (true) {
      print(": ")
      try {
        val p         = postprocessor.poll()
        val evaluated = Mrt.eval(p)
        Mrt.environment.put(s"!$repl".asLISPSymbol, evaluated)
        println(s"!$repl -> $evaluated")
        repl += 1
      } catch {
        case SyntaxException(err) => println(s"Syntax error: $err")
        case EvaluationException(err, symb, _, stack) =>
          println(
            s"Error evaluating $symb: ${err.take(1800)} ${if (err.length > 1800) "..."
            else ""}\n STACK TRACE: \n${RT.fancyFormatStack(stack)}"
          )

        case e: EOFException => println("EOF"); System.exit(0)
        case e: Throwable    => e.printStackTrace()
      }
    }
  }
}
