package cn.librechair.r4rs
import java.{util => ju}

import scala.jdk.CollectionConverters._
import com.github.ghik.silencer.silent

class TreeBuilder[T <: AnyRef] {
  var tmp: ju.LinkedList[AnyRef]                  = new ju.LinkedList()
  var stack: ju.LinkedList[ju.LinkedList[AnyRef]] = new ju.LinkedList()
  def open() {
    if (!stack.isEmpty()) {
      val l = new ju.LinkedList[AnyRef]()
      stack.getLast().add(l)
      stack.add(l)
    } else {
      stack.add(tmp)
    }
  }
  def reset() {
    stack = new ju.LinkedList()
    tmp = new ju.LinkedList()
  }
  def build(): List[AnyRef] = buildi(tmp)

  @silent("type")
  private def buildi(l: ju.LinkedList[AnyRef]): List[AnyRef] =
    l.asScala
      .map(
        s =>
          if (s.isInstanceOf[ju.LinkedList[Any]])
            buildi(s.asInstanceOf[ju.LinkedList[AnyRef]])
          else s
      )
      .toList

  def isOpened = !stack.isEmpty()
  def depth    = stack.size()
  def append(item: T) {
    if (stack.isEmpty()) {
      throw new IllegalStateException("TreeBuilder not opened")
    } else {
      stack.getLast().add(item)
    }
  }
  def stackAppendAll(item: List[Any]) {
    open()
    item.foreach(
      e =>
        if (e.isInstanceOf[List[T]]) stackAppendAll(e.asInstanceOf[List[Any]])
        else append(e.asInstanceOf[T])
    )
    close()
  }
  def close() {
    stack.remove(stack.size() - 1)
  }
}
