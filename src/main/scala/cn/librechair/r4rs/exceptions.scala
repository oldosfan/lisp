package cn.librechair.r4rs

package object exceptions {
  import cn.librechair.r4rs.tokenizer.Token
  case class SyntaxException(val err: String) extends RuntimeException(err)
  case class EvaluationException(
      val err: String,
      val symbol: Token,
      val wrap: Boolean = false,
      val stack: List[(Token, Boolean)] = null
  ) extends RuntimeException(err)
}
