package cn.librechair.r4rs.interpreter
import cn.librechair.r4rs.interpreter.types._
import cn.librechair.r4rs.exceptions.`package`.EvaluationException

final class Environment(
    var environment: Map[LISPSymbol, LISPObject],
    val parent: Environment = null
) {
  private val lock = new AnyRef()

  def putUnrestricted(key: LISPSymbol, value: LISPObject) {
    lock.synchronized {
      environment = environment + (key -> value)
    }
  }

  def put(key: LISPSymbol, value: LISPObject) {
    lock.synchronized {
      if (key.stringp || key.numberp ||
          key.raw == "atom" || key.raw == "nil" || key.raw == "t") {
        throw new EvaluationException(s"Cannot assign to invalid key $key", null)
      }
      environment = environment + (key -> value)
    }
  }

  def get(key: LISPSymbol): Option[LISPObject] = {
    lock.synchronized {
      return environment.get(key)
    }
  }

  def withKv(key: LISPSymbol, value: LISPSymbol): Environment = {
    lock.synchronized {
      val tmp = new Environment(environment)
      tmp.put(key, value)
      return tmp
    }
  }

  def copy: Environment = {
    val copy = new Environment(environment, this)
    return copy
  }

  def copyPreserveParent: Environment =
    new Environment(environment, parent)
}
