package cn.librechair.r4rs.interpreter

import cn.librechair.r4rs.tokenizer._
import cn.librechair.r4rs.interpreter.types._
import cn.librechair.r4rs.interpreter.reflect
import scala.collection.View.Collect
import cn.librechair.r4rs.exceptions._
import java.io.InputStream
import java.io.InputStreamReader
import scala.io.Source
import java.io.StringReader
import java.io.EOFException
import scala.annotation.tailrec
import java.{util => ju}
import java.util.stream.Collector
import com.github.ghik.silencer.silent
import scala.util.Random
import java.util.concurrent.Executors
import java.util.concurrent.Executor
import scala.math.BigDecimal.RoundingMode
import java.math.BigInteger
import java.security.KeyPair

class RT {
  val environment: Environment = new Environment(Map())

  environment.putUnrestricted(new LISPSymbol("nil"), LISPNil)
  environment.putUnrestricted(new LISPSymbol("atom"), LISPObject)
  environment.putUnrestricted(new LISPSymbol("t"), LISPObject)

  reflect.attachToEnvironment(environment)

  environment.put(
    new LISPSymbol("cons"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        new LISPPair(
          e.get(new LISPSymbol("a")).getOrElse(LISPNil),
          e.get(new LISPSymbol("b")).getOrElse(LISPNil)
        )
    )
  )

  environment.put(
    new LISPSymbol("tan"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false)
      ),
      e =>
        (
          Math.tan(e.get(new LISPSymbol("a")).get.asNumber.toDouble)
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("cos"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false)
      ),
      e =>
        (
          Math.cos(e.get(new LISPSymbol("a")).get.asNumber.toDouble)
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("sin"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false)
      ),
      e =>
        (
          Math.sin(e.get(new LISPSymbol("a")).get.asNumber.toDouble)
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("or"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (
          e.get(new LISPSymbol("a")).get.asNumber.longValue |
            e.get(new LISPSymbol("b")).get.asNumber.longValue
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("and"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (
          e.get(new LISPSymbol("a")).get.asNumber.longValue &
            e.get(new LISPSymbol("b")).get.asNumber.longValue
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("shl"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (
          e.get(new LISPSymbol("a")).get.asNumber.longValue <<
            e.get(new LISPSymbol("b")).get.asNumber.longValue
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("xor"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (
          e.get(new LISPSymbol("a")).get.asNumber.longValue ^
            e.get(new LISPSymbol("b")).get.asNumber.longValue
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("shr"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (
          e.get(new LISPSymbol("a")).get.asNumber.longValue >>
            e.get(new LISPSymbol("b")).get.asNumber.longValue
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("set-car!"),
    new LISPFunction(
      List(
        "toset".asLISPSymbol -> ParamInfo(false),
        "val".asLISPSymbol   -> ParamInfo(false)
      ),
      e => {
        e.get("toset".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first = e.get("val".asLISPSymbol).getOrElse(LISPNil)
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("set-cdr!"),
    new LISPFunction(
      List(
        "toset".asLISPSymbol -> ParamInfo(false),
        "val".asLISPSymbol   -> ParamInfo(false)
      ),
      e => {
        e.get("toset".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .rest = e.get("val".asLISPSymbol).getOrElse(LISPNil)
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("eq?"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        if (e.get(new LISPSymbol("a")).getOrElse(LISPNil) eqp
              e.get(new LISPSymbol("b")).getOrElse(LISPNil))
          LISPObject
        else
          LISPNil
    )
  )

  environment.put(
    new LISPSymbol("same?"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        if (e.get(new LISPSymbol("a")).getOrElse(LISPNil) eq
              e.get(new LISPSymbol("b")).getOrElse(LISPNil))
          LISPObject
        else
          LISPNil
    )
  )

  environment.put(
    new LISPSymbol("symbol->string"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false)
      ),
      e =>
        new LISPSymbol(
          "\"" + e
            .get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol]
            .raw + "\""
        )
    )
  )

  environment.put(
    new LISPSymbol("string->symbol"),
    new LISPFunction(
      List(
        new LISPSymbol("string") -> new ParamInfo(false)
      ),
      e =>
        e.get("string".asLISPSymbol)
          .getOrElse(LISPNil)
          .asString
          .asLISPSymbol
    )
  )

  environment.put(
    new LISPSymbol("number->char"),
    new LISPFunction(
      List(
        new LISPSymbol("number") -> new ParamInfo(false)
      ),
      e =>
        new LISPSymbol(
          e.get("number".asLISPSymbol)
            .getOrElse(LISPNil)
            .asNumber
            .toLong
            .asInstanceOf[Char]
            .toString
        )
    )
  )

  environment.put(
    new LISPSymbol("char->number"),
    new LISPFunction(
      List(
        new LISPSymbol("char") -> new ParamInfo(false)
      ),
      e =>
        e.get("char".asLISPSymbol)
          .getOrElse(LISPNil)
          .asString
          .toCharArray()(0)
          .asInstanceOf[Long]
          .asLISPNumber
    )
  )

  environment.put(
    "string->number".asLISPSymbol,
    new LISPFunction(
      List(
        "str".asLISPSymbol -> ParamInfo(false)
      ),
      e =>
        BigDecimal(
          e.get("str".asLISPSymbol)
            .getOrElse(LISPNil)
            .asString
        ).asLISPNumber
    )
  )

  environment.put(
    "symbol->number".asLISPSymbol,
    new LISPFunction(
      List(
        "symb".asLISPSymbol -> ParamInfo(false)
      ),
      e =>
        BigDecimal(
          e.get("symb".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol]
            .raw
        ).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("car"),
    new LISPFunction(
      List(new LISPSymbol("pair") -> new ParamInfo(false)),
      e =>
        e.get(new LISPSymbol("pair"))
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first
    )
  )

  environment.put(
    new LISPSymbol("concurrent/executor"),
    new LISPFunction(
      List(new LISPSymbol("threads") -> new ParamInfo(false)),
      e =>
        new LISPJObject(
          Executors.newFixedThreadPool(
            e.get("threads".asLISPSymbol)
              .getOrElse(LISPNil)
              .asNumber
              .toInt
          )
        )
    )
  )

  environment.put(
    new LISPSymbol("concurrent/post"),
    new LISPFunction(
      List(
        new LISPSymbol("executor")  -> new ParamInfo(false),
        new LISPSymbol("procedure") -> new ParamInfo(false)
      ),
      e => {
        e.get("executor".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPJObject]
          .raw
          .asInstanceOf[Executor]
          .execute(new Runnable {
            override def run() {
              eval(new Collection(List(new Symbol("procedure"))), e)
            }
          })
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("cdr"),
    new LISPFunction(
      List(new LISPSymbol("pair") -> new ParamInfo(false)),
      e =>
        e.get(new LISPSymbol("pair"))
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .rest
    )
  )

  environment.put(
    new LISPSymbol("time"),
    new LISPFunction(
      List(),
      e => BigDecimal(System.currentTimeMillis()).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("eval"),
    new LISPFunction(
      List(
        new LISPSymbol("symb") -> new ParamInfo(false),
        new LISPSymbol("env")  -> new ParamInfo(false, true)
      ),
      e => {
        val symb = e.get("symb".asLISPSymbol).getOrElse(LISPNil)
        val env =
          e.get("env".asLISPSymbol) match {
            case Some(item) =>
              item match {
                case LISPNil => e
                case _       => item.asInstanceOf[LISPPair].asEnvironment(e)
              }
            case None => e
          }
        if (symb.isInstanceOf[LISPSymbol])
          eval(
            new Symbol(symb.asInstanceOf[LISPSymbol].raw),
            env
          )
        else
          eval(
            symb match {
              case p: LISPPair => p.asSymbolList
              case LISPNil     => new Collection(List())
              case _ =>
                throw new EvaluationException("incompatible types", null)
            },
            env
          )
      }
    )
  )

  environment.put(
    "syntax-item".asLISPSymbol,
    new LISPFunction(
      List(
        "args".asLISPSymbol -> new ParamInfo(false),
        "body".asLISPSymbol -> new ParamInfo(true)
      ),
      e => {
        new LISPSyntaxObject(
          if (e.get("body".asLISPSymbol).get != LISPNil)
            e.get("body".asLISPSymbol).get.asInstanceOf[LISPPair].asSymbolList
          else new Collection(List()),
          if (e.get("args".asLISPSymbol).get == LISPNil ||
            (e
              .get("args".asLISPSymbol)
              .get
              .asInstanceOf[LISPPair]
              .asSymbolList
              .tks
              .length == 1 &&
            e
            .get("args".asLISPSymbol)
            .get
            .asInstanceOf[LISPPair]
            .asSymbolList
            .tks.last.raw == "_")) 0 else e
            .get("args".asLISPSymbol)
            .get
            .asInstanceOf[LISPPair]
            .asSymbolList
            .tks
            .length,
          if (e.get("args".asLISPSymbol).get != LISPNil) {
            val ls = e
              .get("args".asLISPSymbol)
              .get
              .asInstanceOf[LISPPair]
              .asSymbolList
              .tks
              .toSeq
              .asInstanceOf[Seq[Symbol]]
              .toList
            SyntaxTokenInfo.buildSTIFromCollection(
              new Collection(
                if (ls.last.raw != "_")
                  ls
                else
                  ls.dropRight(1)
              )
            )
          } else new SyntaxTokenInfo(Seq()),
          (e.get("args".asLISPSymbol).get != LISPNil && e
            .get("args".asLISPSymbol)
            .get
            .asInstanceOf[LISPPair]
            .asSymbolList
            .tks
            .toSeq
            .asInstanceOf[Seq[Symbol]]
            .last
            .raw == "_")
        )
      },
      true
    )
  )

  environment.put(
    new LISPSymbol("plus"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first +
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    new LISPSymbol("mod"),
    new LISPFunction(
      List(
        new LISPSymbol("a") -> new ParamInfo(false),
        new LISPSymbol("b") -> new ParamInfo(false)
      ),
      e =>
        (e.get("a".asLISPSymbol)
          .getOrElse(LISPNil)
          .asNumber %
          e.get("b".asLISPSymbol)
            .getOrElse(LISPNil)
            .asNumber).asLISPNumber
    )
  )

  environment.put(
    new LISPSymbol("more-than?"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first >
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    new LISPSymbol("less-than?"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first <
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    "string-split".asLISPSymbol,
    new LISPFunction(
      List("str".asLISPSymbol -> ParamInfo(false)),
      e =>
        LISPPair(
          e.get("str".asLISPSymbol)
            .getOrElse(LISPNil)
            .asString
            .toCharArray()
            .map(c => LISPSymbol(c.toString))
            .toList
        )
    )
  )

  environment.put(
    new LISPSymbol("minus"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first -
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    new LISPSymbol("product"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first *
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    new LISPSymbol("error"),
    new LISPFunction(
      List(
        new LISPSymbol("str") -> new ParamInfo(false)
      ),
      e =>
        throw new EvaluationException(
          e.get("str".asLISPSymbol).getOrElse(LISPNil).asString,
          null
        )
    )
  )

  environment.put(
    new LISPSymbol("quotient"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .first /
          e.get("params".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPPair]
            .rest
            .asInstanceOf[LISPPair]
    )
  )

  environment.put(
    new LISPSymbol("fn"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(false),
        new LISPSymbol("body")   -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol) match {
          case Some(params) =>
            e.get("body".asLISPSymbol) match {
              case Some(symb) =>
                symb match {
                  case p2: LISPPair =>
                    new LISPFunction(
                      params match {
                        case p: LISPPair => {
                          val _l = p.asList
                          val opt = _l.indexWhere(_.asInstanceOf[LISPSymbol].raw == "/optional")
                          val l = if (opt >= 0) _l.patch(opt, Nil, 1) else _l
                          l.zipWithIndex
                            .map {
                              case (s, e) =>
                                s.asInstanceOf[LISPSymbol] -> new ParamInfo(
                                  s.asInstanceOf[LISPSymbol].raw.endsWith("--"),
                                  e >= opt && opt != -1
                                )
                            }
                        }
                        case LISPNil => Nil
                        case _ =>
                          throw new EvaluationException(
                            "expected list but got something else",
                            null
                          )
                      },
                      e => {
                        val exps = p2.asList
                        for (ex <- exps.dropRight(1)) {
                          if (ex.isInstanceOf[LISPSymbol]) {
                            eval(
                              new Symbol(ex.asInstanceOf[LISPSymbol].raw),
                              e
                            )
                          } else {
                            eval(
                              if (ex.isInstanceOf[LISPPair])
                                ex.asInstanceOf[LISPPair].asSymbolList
                              else
                                new Collection(List()),
                              e
                            )
                          }
                        }
                        LISPNil
                      },
                      false,
                      Option(e.parent),
                      p2
                    )
                  case _ => throw new EvaluationException("invalid type", null)
                }
              case _ => LISPNil
            }
          case _ => LISPNil
        },
      true
    )
  )

  environment.put(
    new LISPSymbol("lambda"),
    new LISPFunction(
      List(
        new LISPSymbol("params") -> new ParamInfo(false),
        new LISPSymbol("body")   -> new ParamInfo(true)
      ),
      e =>
        e.get("params".asLISPSymbol) match {
          case Some(params) =>
            e.get("body".asLISPSymbol) match {
              case Some(symb) =>
                symb match {
                  case p2: LISPPair =>
                    new LISPFunction(
                      params match {
                        case p: LISPPair =>
                          p.asList
                            .map(
                              s =>
                                s.asInstanceOf[LISPSymbol] -> new ParamInfo(
                                  s.asInstanceOf[LISPSymbol].raw.endsWith("--")
                                )
                            )
                        case LISPNil => Nil
                        case _ =>
                          throw new EvaluationException(
                            "expected list but got something else",
                            null
                          )
                      },
                      e => {
                        val exps = p2.asList
                        for (ex <- exps.dropRight(1)) {
                          if (ex.isInstanceOf[LISPSymbol]) {
                            eval(
                              new Symbol(ex.asInstanceOf[LISPSymbol].raw),
                              e
                            )
                          } else {
                            eval(
                              if (ex.isInstanceOf[LISPPair])
                                ex.asInstanceOf[LISPPair].asSymbolList
                              else
                                new Collection(List()),
                              e
                            )
                          }
                        }
                        LISPNil
                      },
                      false,
                      None,
                      p2
                    )
                  case _ => throw new EvaluationException("invalid type", null)
                }
              case _ => LISPNil
            }
          case _ => LISPNil
        },
      true
    )
  )

  environment.put(
    "procedure->definition".asLISPSymbol,
    new LISPFunction(
      List("func".asLISPSymbol -> ParamInfo(false)),
      e =>
        e.get("func".asLISPSymbol).getOrElse(LISPNil) match {
          case LISPNil                        => LISPNil
          case LISPFunction(_, _, _, _, symb) => symb
          case _ =>
            throw new EvaluationException("type mismatch", null)
        }
    )
  )

  environment.put(
    "load-file".asLISPSymbol,
    new LISPFunction(
      List("file".asLISPSymbol -> ParamInfo(false)),
      e => {
        loadFile(
          getClass()
            .getClassLoader()
            .getResourceAsStream(
              e.get("file".asLISPSymbol)
                .getOrElse(LISPNil)
                .asString
            )
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("assign"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false),
        new LISPSymbol("item")   -> new ParamInfo(false)
      ),
      e => {
        if (e.parent.environment.contains(
              e.get("symbol".asLISPSymbol)
                .getOrElse(LISPNil)
                .asInstanceOf[LISPSymbol]
            )) {
          throw new EvaluationException(
            "Environment already contains symbol" +
              s" ${e.get("symbol".asLISPSymbol).getOrElse(LISPNil)}",
            null
          )
        }
        e.parent.put(
          e.get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol],
          e.get("item".asLISPSymbol).getOrElse(LISPNil)
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("assign*"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false),
        new LISPSymbol("item")   -> new ParamInfo(false)
      ),
      e => {
        if (e.parent.parent.environment.contains(
              e.get("symbol".asLISPSymbol)
                .getOrElse(LISPNil)
                .asInstanceOf[LISPSymbol]
            )) {
          throw new EvaluationException(
            "Environment already contains symbol" +
              s" ${e.get("symbol".asLISPSymbol).getOrElse(LISPNil)}",
            null
          )
        }
        e.parent.parent.put(
          e.get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol],
          e.get("item".asLISPSymbol).getOrElse(LISPNil)
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("assign*!"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false),
        new LISPSymbol("item")   -> new ParamInfo(false)
      ),
      e => {
        e.parent.parent.put(
          e.get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol],
          e.get("item".asLISPSymbol).getOrElse(LISPNil)
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("string->jobject"),
    new LISPFunction(
      List(
        new LISPSymbol("string") -> new ParamInfo(false)
      ),
      e => new LISPJObject(e.get("string".asLISPSymbol).get.asString)
    )
  )

  environment.put(
    new LISPSymbol("environment"),
    new LISPFunction(
      List(),
      e =>
        LISPPair(
          e.environment.map(entry => new LISPPair(entry._1, entry._2)).toList
        )
    )
  )

  environment.put(
    "floor".asLISPSymbol,
    new LISPFunction(
      List("number".asLISPSymbol -> ParamInfo(false)),
      e =>
        e.get("number".asLISPSymbol)
          .getOrElse(LISPNil)
          .asNumber
          .setScale(0, RoundingMode.FLOOR)
          .asLISPNumber
    )
  )

  environment.put(
    "random".asLISPSymbol,
    new LISPFunction(
      List(
        "lower".asLISPSymbol -> ParamInfo(false),
        "upper".asLISPSymbol -> ParamInfo(false)
      ),
      e =>
        Random
          .between(
            e.get("lower".asLISPSymbol)
              .getOrElse(LISPNil)
              .asNumber
              .toDouble,
            e.get("upper".asLISPSymbol)
              .getOrElse(LISPNil)
              .asNumber
              .toDouble
          )
          .asLISPNumber
    )
  )

  environment.put("jobject->symbol".asLISPSymbol,
    new LISPFunction(
      List("int".asLISPSymbol -> ParamInfo(false)),
      e => e.get("int".asLISPSymbol)
        .get.asInstanceOf[LISPJObject].raw match {
          case str: String => new LISPSymbol("\"" + str + "\"")
          case num: BigInteger => new LISPSymbol(num.toString)
          case num: java.math.BigDecimal => new LISPSymbol(num.toPlainString())
          case num: BigDecimal => new LISPSymbol(num.bigDecimal.toPlainString())
          case e => new LISPSymbol(e.toString())
         }
    )
  )

  environment.put(
    new LISPSymbol("parent-environment"),
    new LISPFunction(
      List(),
      e =>
        LISPPair(
          e.parent.parent.environment
            .map(entry => new LISPPair(entry._1, entry._2))
            .toList
        )
    )
  )

  environment.put(
    new LISPSymbol("environment-ref"),
    new LISPFunction(
      List(),
      e => new LISPJObject(e.parent)
    )
  )
  environment.put(
    new LISPSymbol("parent-environment-ref"),
    new LISPFunction(
      List(),
      e => new LISPJObject(e.parent.parent)
    )
  )

  environment.put(
    new LISPSymbol("assign"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false),
        new LISPSymbol("item")   -> new ParamInfo(false)
      ),
      e => {
        if (e.parent.environment.contains(
              e.get("symbol".asLISPSymbol)
                .getOrElse(LISPNil)
                .asInstanceOf[LISPSymbol]
            )) {
          throw new EvaluationException(
            "Environment already contains symbol" +
              s" ${e.get("symbol".asLISPSymbol).getOrElse(LISPNil)}",
            null
          )
        }
        e.parent.put(
          e.get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol],
          e.get("item".asLISPSymbol).getOrElse(LISPNil)
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("assign!"),
    new LISPFunction(
      List(
        new LISPSymbol("symbol") -> new ParamInfo(false),
        new LISPSymbol("item")   -> new ParamInfo(false)
      ),
      e => {
        e.parent.put(
          e.get("symbol".asLISPSymbol)
            .getOrElse(LISPNil)
            .asInstanceOf[LISPSymbol],
          e.get("item".asLISPSymbol).getOrElse(LISPNil)
        )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("assign-ref!"),
    new LISPFunction(
      List(
        new LISPSymbol("env-object") -> new ParamInfo(false),
        new LISPSymbol("symbol")     -> new ParamInfo(false),
        new LISPSymbol("item")       -> new ParamInfo(false)
      ),
      e => {
        e.get("env-object".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPJObject]
          .raw
          .asInstanceOf[Environment]
          .put(
            e.get("symbol".asLISPSymbol)
              .getOrElse(LISPNil)
              .asInstanceOf[LISPSymbol],
            e.get("item".asLISPSymbol).getOrElse(LISPNil)
          )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("expand-syntax-item"),
    new LISPFunction(
      List(
        new LISPSymbol("object")  -> new ParamInfo(false),
        new LISPSymbol("symbols") -> new ParamInfo(true)
      ),
      e => {
        val expanded = macroExpand(
          e.get("object".asLISPSymbol).get.asInstanceOf[LISPSyntaxObject],
          e.get("symbols".asLISPSymbol).get match {
            case p: LISPPair => p.asSymbolList.tks
            case LISPNil     => Nil
            case _           => ???
          }
        )
        if (expanded.isInstanceOf[Symbol])
          expanded.raw.asLISPSymbol
        else
          expanded.asInstanceOf[Collection].asLisp
      }
    )
  )

  environment.put(
    new LISPSymbol("jobject?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .isInstanceOf[LISPJObject]) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("number?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .numberp) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("syntax-item?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .isInstanceOf[LISPSyntaxObject]) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("string?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .stringp) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("procedure?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .isInstanceOf[LISPFunction]) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("display"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .stringp)
          println(
            e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .asString
          )
        else
          println(
            e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
          )
        LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("read"),
    new LISPFunction(List(), e => {
      val postprocessor =
        new Postprocessor(new Tokenizer(new InputStreamReader(System.in)))
      val polled = postprocessor.poll()
      if (polled.isInstanceOf[Symbol]) {
        polled.asSymbol.asLISPSymbol
      } else {
        polled.asInstanceOf[Collection].asLisp
      }
    })
  )

  environment.put(
    new LISPSymbol("symbol?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .symbolp) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("cons?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .isInstanceOf[LISPPair]) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("circular-list?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .isInstanceOf[LISPPair] &&
            e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .asInstanceOf[LISPPair]
              .isCircular) LISPObject
        else
          LISPNil
      }
    )
  )

  environment.put(
    new LISPSymbol("slice-circular-list"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e =>
        e.get("item".asLISPSymbol)
          .getOrElse(LISPNil)
          .asInstanceOf[LISPPair]
          .getUniqueSliceFromCircularList(Vector(), LISPNil)
    )
  )

  environment.put(
    new LISPSymbol("nil?"),
    new LISPFunction(
      List(
        new LISPSymbol("item") -> new ParamInfo(false)
      ),
      e => {
        if (e.get("item".asLISPSymbol)
              .getOrElse(LISPNil)
              .nilp) LISPObject
        else
          LISPNil
      }
    )
  )

  @silent("unchecked")
  def eval(
      _token: Token,
      _env: Environment = environment,
    oe: LISPFunction = null,
    os: List[(Token, Boolean)] = List()
  ): LISPObject = {
    var stack = os.appended((_token, false))
    var debugToken: Token         = null
    var eh: List[LISPFunction]     = Nil
    var ehTrigger: Exception       = null
    var tcexpeval: Token           = null
    var tcexpenv: Environment      = null
    var overrideEval: LISPFunction = null

    while (true) {
      try {
        val token = if (tcexpeval != null) tcexpeval else _token
        debugToken = token

        val env = if (tcexpenv != null) tcexpenv else _env
        tcexpenv = null
        tcexpeval = null
        if (token.isInstanceOf[Symbol]) {
          val raw = token.asInstanceOf[Symbol].raw
          if (raw.matches("^-?\\d+(\\.\\d+)?$")) {
            return raw.asLISPSymbol
          } else if (raw.startsWith("\"") && raw.endsWith("\"")) {
            return raw.asLISPSymbol
          } else {
            return env.get(LISPSymbol(raw)).getOrElse(LISPNil)
          }
        } else {
          var symbol: Collection = null
          if (token != null) {
            symbol = token.asInstanceOf[Collection]
          }
    
          if (symbol != null && symbol.nil) {
            return LISPNil
          } else {
            /*
             * Warning: a Super-spaghettifier 500 has been through this code
             */

            if (eh == Nil && ehTrigger != null) {
              throw ehTrigger
            }

            if (token.isInstanceOf[Collection] &&
              token.asInstanceOf[Collection].pair) {
              throw new EvaluationException("trying to evaluate pair", null)
            }


            val he = (ehTrigger != null && eh != Nil)
            val hx = if (he) eh.head else null
            if (he) {
              eh = Nil
              ehTrigger = null
            }

            val additionalTokens = if (he) List[Token]() else symbol.tks.tail
            val fn               = if (he) new Symbol("") else symbol.tks.head

            if (fn.raw == "apply") {
              tcexpeval = new Collection(
                List(additionalTokens(0)).appendedAll(
                  eval(additionalTokens(1), env, os = stack)
                    .asInstanceOf[LISPPair]
                    .asSymbolList
                    .tks
                )
              )
              tcexpenv = env
            } else if (fn.raw == "quote") {
              if (additionalTokens.size != 1) {
                throw new EvaluationException("invalid call to quote", null)
              }
              if (additionalTokens(0).isInstanceOf[Symbol]) {
                return additionalTokens(0).raw.asLISPSymbol
              } else {
                return additionalTokens(0).asInstanceOf[Collection].asLisp
              }
            } else if (fn.raw == "uw-protect") {
              eh = eval(additionalTokens.head, env)
                .asInstanceOf[LISPFunction] :: eh
              for (x <- additionalTokens.tail) {
                if (x eq additionalTokens.last) {
                  tcexpenv = env
                  tcexpeval = x
                  stack = stack.appended((tcexpeval, true))
                } else {
                  eval(x, env, os = stack)
                }
              }
            } else if (fn.raw == "if") {
              val pred = eval(additionalTokens(0), env)
              if (pred != LISPNil) {
                tcexpeval = additionalTokens(1)
              } else {
                if (additionalTokens.size > 2) {
                  tcexpeval = additionalTokens(2)
                  stack = stack.appended((tcexpeval, true))
                } else {
                  return LISPNil
                }
              }
              tcexpenv = env
            } else if (fn.raw == "get-stack") {
              return LISPPair(stack.map(e =>
                     new LISPPair(e._1.asLisp, if (e._2) LISPObject else LISPNil)))
            } else if (fn.raw == "let") {
              val envtmp = env.copy
              additionalTokens.head
                .asInstanceOf[Collection]
                .tks
                .foreach(
                  i =>
                    envtmp.put(
                      i.asInstanceOf[Collection].tks.head.raw.asLISPSymbol,
                      eval(i.asInstanceOf[Collection].tks(1), envtmp, os = stack)
                    )
                )
              additionalTokens.drop(1).dropRight(1).foreach(eval(_, envtmp, os = stack))
              tcexpenv = envtmp
              tcexpeval = additionalTokens.last
              stack = stack.appended((tcexpeval, true))
            } else if (fn.raw == "let*") {
              val envtmp = env.copyPreserveParent
              additionalTokens.head
                .asInstanceOf[Collection]
                .tks
                .foreach(
                  i =>
                    envtmp.put(
                      i.asInstanceOf[Collection].tks.head.raw.asLISPSymbol,
                      eval(i.asInstanceOf[Collection].tks(1), envtmp, os = stack)
                    )
                )
              additionalTokens.drop(1).dropRight(1).foreach(eval(_, envtmp))
              tcexpenv = envtmp
              tcexpeval = additionalTokens.last
              stack = stack.appended((tcexpeval, true))
            } else if (fn.raw == "and?") {
              return if (additionalTokens.forall(!eval(_, env, os = stack).nilp))
                LISPObject
              else LISPNil
            } else if (fn.raw == "or?") {
              return if (additionalTokens.find(!eval(_, env, os = stack).nilp) != None)
                LISPObject
              else LISPNil
            } else if (fn.raw == "fn" && additionalTokens.size == 1) {
              return new LISPFunction(
                List("o".asLISPSymbol -> ParamInfo(false)),
                null,
                false,
                Some(env),
                new LISPPair(
                  if (additionalTokens.head.isInstanceOf[Collection])
                    additionalTokens(0).asInstanceOf[Collection].asLisp
                  else
                    additionalTokens(0).raw.asLISPSymbol,
                  LISPNil
                )
              )
            } else if (fn.raw == "cond") {
              additionalTokens
                .map(_.asInstanceOf[Collection].tks)
                .foreach(
                  p =>
                    if (eval(p.head, env, os = stack) != LISPNil) {
                      tcexpenv = env
                      p.tail.dropRight(1).foreach(eval(_, env, os = stack))
                      tcexpeval = p.tail.last
                      stack = stack.appended((tcexpeval, true))
                    }
                )
            } else {
              val _fun = if (hx != null) hx else eval(fn, env, os = stack)
              if (!_fun.isInstanceOf[LISPFunction] && !_fun
                    .isInstanceOf[LISPSyntaxObject]) {
                throw new EvaluationException(
                  s"cannot apply to ${_fun}",
                  null
                )
              } else if (_fun.isInstanceOf[LISPFunction]) {
                val fun       = _fun.asInstanceOf[LISPFunction]
                val hasOptional = fun.params.find(_._2.optional) != None
                val hasVararg = !fun.params.isEmpty && fun.params.last._2.vararg
                var argOverride: List[LISPObject] = null
                val firstOptional = fun.params.indexWhere(_._2.optional)
        
                val adjustedParamSize =
                  if (!hasOptional) fun.params.size - (if (hasVararg) 1 else 0) else firstOptional
                val adjustedParamIndex = adjustedParamSize - 1
                val optionalMax = fun.params.size - (if (hasVararg) 2 else 1)

                if (additionalTokens.size > 0) {
                  if (additionalTokens(0).isInstanceOf[Collection] &&
                      additionalTokens(0)
                        .asInstanceOf[Collection]
                        .tks
                        .size > 0 &&
                      additionalTokens(0)
                        .asInstanceOf[Collection]
                        .tks(0)
                        .raw == "expand") {
                    argOverride = eval(
                      additionalTokens(0)
                        .asInstanceOf[Collection]
                        .tks(1),
                      env
                    ).asInstanceOf[LISPPair].asList
                  }
                }

                val tksz =
                  if (argOverride != null) argOverride.size
                  else additionalTokens.size

                if (tksz < adjustedParamSize
                    || (!hasVararg && tksz > fun.params.size)) {
                  throw new EvaluationException(
                    s"cannot apply to $fn ($fun) because of an argument count mismatch",
                    null
                  )
                }
                val envr = fun.env.getOrElse(env).copy
                for (i <- 0 to adjustedParamIndex) {
                  if (!fun.macrop) {
                    envr.put(
                      fun.params(i)._1,
                      if (argOverride != null) argOverride(i)
                      else eval(additionalTokens(i), env, os = stack)
                    )
                  } else {
                    envr.put(
                      fun.params(i)._1,
                      if (additionalTokens(i).isInstanceOf[Symbol])
                        additionalTokens(i).asSymbol.asLISPSymbol
                      else
                        additionalTokens(i).asInstanceOf[Collection].asLisp
                    )
                  }
                }
                if (hasVararg) {
                  envr.put(
                    fun.params.last._1,
                    LISPPair(
                      if (argOverride == null)
                        additionalTokens
                          .slice(
                            fun.params.length - 1,
                            additionalTokens.length
                          )
                          .map(
                            i =>
                              if (i.isInstanceOf[Collection] && !i
                                    .asInstanceOf[Collection]
                                    .tks
                                    .isEmpty && i
                                    .asInstanceOf[Collection]
                                    .tks
                                    .head == new Symbol("unpack"))
                                eval(
                                  i.asInstanceOf[Collection].tks.tail.head,
                                  env,
                                  os = stack
                                ) match {
                                  case p: LISPPair => p.asList
                                  case LISPNil     => Nil
                                  case _           => ???
                                } else if (!fun.macrop) eval(i, env, os = stack)
                              else if (i.isInstanceOf[Symbol])
                                i.asSymbol.asLISPSymbol
                              else
                                i.asInstanceOf[Collection].asLisp
                          )
                          .map(
                            i =>
                              if (i.isInstanceOf[List[LISPObject]]) i
                              else List(i)
                          )
                          .map(_.asInstanceOf[List[LISPObject]])
                          .flatten
                          .toList
                      else
                        argOverride
                          .slice(fun.params.length - 1, argOverride.length)
                    )
                  )
                }

                if (hasOptional) {
                  for (i <- firstOptional to optionalMax) {
                    envr.put(fun.params(i)._1, LISPNil)
                  }
                  if (argOverride != null && argOverride != Nil) {
                    argOverride.zipWithIndex.foreach {
                      case ((r, i)) => if (i >= firstOptional && i <= optionalMax) {
                        envr.put(fun.params(i)._1, r)
                      }
                    }
                  } else {
                     additionalTokens.zipWithIndex.foreach {
                      case ((tk, i)) => if (i >= firstOptional && i <= optionalMax) {
                        envr.put(fun.params(i)._1, eval(tk, env, os = stack))
                      }
                    }
                  }
                }

                if (fun.isSymbols == LISPNil) {
                  return fun.apply(envr)
                } else {
                  fun.isSymbols.asInstanceOf[LISPPair].asList.dropRight(1)
                    .foreach(c => eval(c match {
                      case LISPSymbol(raw) => new Symbol(raw)
                      case p: LISPPair =>
                        p.asSymbolList
                      case _ =>
                        throw new IllegalStateException(
                          s"last in ${fun.isSymbols} is not either a symbol or a pair"
                        )
                    }, envr, os = stack))
                  tcexpeval =
                    fun.isSymbols.asInstanceOf[LISPPair].asList.last match {
                      case LISPSymbol(raw) => new Symbol(raw)
                      case p: LISPPair =>
                        p.asSymbolList
                      case _ =>
                        throw new IllegalStateException(
                          s"last in ${fun.isSymbols} is not either a symbol or a pair"
                        )
                    }
                  tcexpenv = envr
                  stack = stack.appended((tcexpeval, true))
                }
              } else {
                val fn        = _fun.asInstanceOf[LISPSyntaxObject]
                val expndEval = macroExpand(fn, additionalTokens)
                if (expndEval.isInstanceOf[Collection]) {
                  val eetks = expndEval.asInstanceOf[Collection].tks
                  if (!eetks.isEmpty) {
                    eetks.dropRight(1).foreach(eval(_, env))
                    tcexpenv = env
                    tcexpeval = eetks.last
                    stack = stack.appended((tcexpeval, true))
                  } else {
                    return LISPNil
                  }
                }
              }
            }
          }
        }
      } catch {
        case EvaluationException(err, tk, false, _) => {
          ehTrigger = new EvaluationException(
            err,
            if (tk == null) debugToken else tk,
            true,
            stack
          )
        }
        case e: EvaluationException => throw e
        case e: Throwable => {
          ehTrigger = new EvaluationException(
            s"${e.getClass()}: ${e.getMessage()} (\n${e.getStackTrace().mkString("\n")}",
            debugToken,
            true,
            stack
          )
        }
      }
    }
    return LISPNil
  }

  private def macroExpand(
      fn: LISPSyntaxObject,
      additionalTokens: List[Token]
  ): Token = {
    try {
      val processedTokens = additionalTokens.slice(0, fn.realtc)
      return LISPSyntaxObject.expand(
        fn,
        SyntaxTokenInfo
          .getPartabFromTokensAndSTI(fn.syml, processedTokens.toSeq),
        additionalTokens
          .slice(processedTokens.size - 1, additionalTokens.size)
      )
    } catch {
      case _: IndexOutOfBoundsException =>
        throw new EvaluationException("Error expanding syntax item: index out of bounds", null)
    }
  }

  def loadFile(i: InputStream) {
    val string        = Source.fromInputStream(i).mkString
    val reader        = new StringReader(string)
    val tokenizer     = new Tokenizer(reader)
    val postprocessor = new Postprocessor(tokenizer)
    try {
      while (true) {
        val p = postprocessor.poll()
        eval(p)
      }
    } catch {
      case e: EOFException => return
    }
  }
}

object Mrt extends RT()

object RT {
  def fancyFormatStack(stack: List[(Token, Boolean)]) = 
    "    " + stack.reverse.map(p =>
      p._1.toString.take(40) + " ".repeat(40 - p._1.toString.take(40).size) +
        (if (p._2) "   [LIFTED OUT]" else "")).mkString("\n    ")
}
