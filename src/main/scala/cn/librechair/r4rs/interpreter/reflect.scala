package cn.librechair.r4rs.interpreter

import cn.librechair.r4rs.interpreter.types._

package object reflect {

  import java.{util => ju}
  import java.lang.reflect.Field
  import java.lang.reflect.Constructor
  import java.lang.reflect.Method
  import cn.librechair.r4rs.exceptions.`package`.EvaluationException

  def attachToEnvironment(ev: Environment) {
    ev.put("attach-reflect-module".asLISPSymbol, new LISPFunction(Nil, e => {
      attachToEnvironmentInternal(ev)
      LISPNil
    }))
  }

  def attachToEnvironmentInternal(e: Environment) {
    e.put(
      "rf:get-class".asLISPSymbol,
      new LISPFunction(
        List("name".asLISPSymbol -> ParamInfo(false)),
        e =>
          Class.forName(e.get("name".asLISPSymbol).get.asString).asLISPJObject
      )
    )
    e.put(
      "rf:get-pclass".asLISPSymbol,
      new LISPFunction(
        List("name".asLISPSymbol -> ParamInfo(false)),
        e =>
          (e.get("name".asLISPSymbol).get.asInstanceOf[LISPSymbol].raw match {
            case "boolean" => classOf[Boolean]
            case "int"     => classOf[Integer]
            case "byte"    => classOf[Byte]
            case "char"    => classOf[Char]
            case "long"    => classOf[Long]
            case "double"  => classOf[Double]
            case "float"   => classOf[Float]
          }).asLISPJObject
      )
    )

    e.put(
      "rf:get-array-class".asLISPSymbol,
      new LISPFunction(
        List("name".asLISPSymbol -> ParamInfo(false)),
        e =>
          java.lang.reflect.Array
            .newInstance(
              e.get("name".asLISPSymbol)
                .collect {
                  case LISPJObject(e: Class[_]) => e
                  case _                        => null
                }
                .get,
              0
            )
            .getClass()
            .asLISPJObject
      )
    )

    e.put(
      "rf:array-of-one".asLISPSymbol,
      new LISPFunction(
        List("f".asLISPSymbol -> ParamInfo(false)),
        e => {
          val ins = e.get("f".asLISPSymbol).get.asInstanceOf[LISPJObject].raw
          val dc = java.lang.reflect.Array.newInstance(ins.getClass(), 1)
          java.lang.reflect.Array.set(dc, 0, ins)
          dc.asLISPJObject
        }
      )
    )

    e.put(
      "rf:get-constructor".asLISPSymbol,
      new LISPFunction(
        List(
          "class".asLISPSymbol  -> ParamInfo(false),
          "qwargs".asLISPSymbol -> ParamInfo(true)
        ),
        e => {
          val clas: Class[_] = e
            .get("class".asLISPSymbol)
            .get
            .asInstanceOf[LISPJObject]
            .raw
            .asInstanceOf[Class[_]]
          val args: Array[Class[_]] = e.get("qwargs".asLISPSymbol).get match {
            case LISPNil => Array[Class[_]]()
            case p: LISPPair =>
              p.asList
                .map(_.asInstanceOf[LISPJObject].raw.asInstanceOf[Class[_]])
                .toArray
            case _ => throw new EvaluationException("wrong type", null)
          }

          clas.getConstructor(args: _*).asLISPJObject
        }
      )
    )

    e.put(
      "rf:get-method".asLISPSymbol,
      new LISPFunction(
        List(
          "class".asLISPSymbol  -> ParamInfo(false),
          "name".asLISPSymbol   -> ParamInfo(true),
          "qwargs".asLISPSymbol -> ParamInfo(true)
        ),
        e => {
          val clas: Class[_] = e
            .get("class".asLISPSymbol)
            .get
            .asInstanceOf[LISPJObject]
            .raw
            .asInstanceOf[Class[_]]
          val name = e.get("name".asLISPSymbol).get.asString
          val args: Array[Class[_]] = e.get("qwargs".asLISPSymbol).get match {
            case LISPNil => Array[Class[_]]()
            case p: LISPPair =>
              p.asList
                .map(_.asInstanceOf[LISPJObject].raw.asInstanceOf[Class[_]])
                .toArray
            case _ => throw new EvaluationException("wrong type", null)
          }

          clas.getDeclaredMethod(name, args: _*).asLISPJObject
        }
      )
    )

    e.put(
      "rf:invoke-method".asLISPSymbol,
      new LISPFunction(
        List(
          "method".asLISPSymbol -> ParamInfo(false),
          "obj".asLISPSymbol    -> ParamInfo(false),
          "args".asLISPSymbol   -> ParamInfo(true)
        ),
        e => {
          val method = e
            .get("method".asLISPSymbol)
            .get
            .asInstanceOf[LISPJObject]
            .raw
          val obj = e.get("obj".asLISPSymbol) match {
            case Some(LISPNil) | None     => null
            case Some(LISPJObject(value)) => value
            case _                        => throw new EvaluationException("wrong type", null)
          }
          val args = e.get("args".asLISPSymbol).get match {
            case LISPNil => Array[Any]()
            case p: LISPPair =>
              p.asList.map(_.asInstanceOf[LISPJObject].raw).toArray
            case _ => throw new EvaluationException("wrong type", null)
          }

          if (method.isInstanceOf[Constructor[_]])
            method
              .asInstanceOf[Constructor[_]]
              .newInstance(args: _*)
              .asLISPJObject
          else method.asInstanceOf[Method].invoke(obj, args: _*).asLISPJObject
        }
      )
    )

    e.put(
      "rf:to-java".asLISPSymbol,
      new LISPFunction(
        List(
          "arg".asLISPSymbol    -> ParamInfo(false),
          "coerce".asLISPSymbol -> ParamInfo(false, true)
        ),
        e =>
          toJava(
            e.get("arg".asLISPSymbol),
            e.get("coerce".asLISPSymbol).get.asInstanceOf[LISPSymbol]
          )
      )
    )

    e.put(
      "rf:get-field".asLISPSymbol,
      new LISPFunction(
        List(
          "field".asLISPSymbol  -> ParamInfo(false),
          "object".asLISPSymbol -> ParamInfo(false)
        ),
        e =>
          e.get("field".asLISPSymbol) match {
            case None => LISPNil
            case Some(LISPJObject(f: Field)) =>
              f.get(
                  e.get("object".asLISPSymbol).get match {
                    case LISPJObject(raw) => f.get(raw)
                    case LISPNil          => f.get(null)
                    case _                => ???
                  }
                )
                .asLISPJObject
            case _ => ???
          }
      )
    )

    e.put(
      "rf:find-field".asLISPSymbol,
      new LISPFunction(
        List(
          "class".asLISPSymbol -> ParamInfo(false),
          "name".asLISPSymbol  -> ParamInfo(false)
        ),
        e => {
          val clazz: Class[_] = e
            .get("class".asLISPSymbol)
            .get
            .asInstanceOf[LISPJObject]
            .raw
            .asInstanceOf[Class[_]]
          clazz
            .getField(e.get("name".asLISPSymbol).get.asString)
            .asLISPJObject
        }
      )
    )
  }

  def toJava(i: Option[LISPObject], coerce: LISPSymbol): LISPJObject = i match {
    case None => new LISPJObject(null)
    case Some(LISPNil) =>
      if (coerce.raw == "bool")
        new LISPJObject(null)
      else false.asLISPJObject
    case Some(s: LISPSymbol) => {
      if (s.numberp) {
        if (coerce.raw == "byte")
          s.asNumber.toByteExact.asLISPJObject
        else if ((s.asNumber.isValidInt &&
                 coerce.raw == "nil") || coerce.raw == "int")
          s.asNumber.toIntExact.asLISPJObject
        else if ((s.asNumber.isValidLong &&
                 coerce.raw == "nil") || coerce.raw == "long")
          s.asNumber.toLongExact.asLISPJObject
        else if (s.asNumber.isDecimalDouble)
          s.asNumber.toDouble.asLISPJObject
        else
          s.asNumber.bigDecimal.asLISPJObject
      } else if (s.stringp) {
        s.asString.asLISPJObject
      } else if (s == LISPObject) {
        true.asLISPJObject
      } else if (s == LISPNil && coerce.raw == "boolean") {
        false.asLISPJObject
      } else {
        s.asLISPJObject
      }
    }
    case Some(s: LISPJObject) => s
    case Some(p: LISPPair) =>
      p.asList.map(a => toJava(Some(a), coerce).raw).asLISPJObject
    case Some(s: LISPObject) => new LISPJObject(s)
  }
}
