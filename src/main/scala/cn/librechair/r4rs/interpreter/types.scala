package cn.librechair.r4rs.interpreter

package object types {

  import cn.librechair.r4rs.tokenizer.Token
  import java.{util => ju}
  import java.{util => ju}
  import java.{util => ju}
  import scala.annotation.tailrec
  import java.{util => ju}
  import scala.collection.View.Collect
  import cn.librechair.r4rs.tokenizer._
  import cn.librechair.r4rs.tokenizer.Symbol
  import cn.librechair.r4rs.exceptions._
  import scala.collection.JavaConverters._

  sealed abstract class LISPObject {
    def asNumber: BigDecimal =
      if (!numberp) throw new IllegalArgumentException("Not a number")
      else BigDecimal(this.asInstanceOf[LISPSymbol].raw)
    def asToken: Token = new Symbol(toString)
    def asString: String =
      if (!stringp)
        throw new IllegalArgumentException(s"$this is not a string")
      else
        this
          .asInstanceOf[LISPSymbol]
          .raw
          .substring(1, this.asInstanceOf[LISPSymbol].raw.length() - 1)
    def nilp =
      (this eq LISPNil) || this
        .isInstanceOf[LISPSymbol] && this.asInstanceOf[LISPSymbol].raw == "nil"
    def symbolp = !numberp && !stringp
    def stringp =
      this.isInstanceOf[LISPSymbol] && this
        .asInstanceOf[LISPSymbol]
        .raw
        .startsWith("\"") && this.asInstanceOf[LISPSymbol].raw.endsWith("\"")
    def numberp =
      this.isInstanceOf[LISPSymbol] && this
        .asInstanceOf[LISPSymbol]
        .raw
        .matches("^-?\\d+(\\.\\d+)?$")
    def eqp(other: LISPObject): Boolean = {
      if (other.nilp && this.nilp) {
        return true
      } else if (other eq this) {
        return true
      } else if (other.isInstanceOf[LISPJObject] && this
                   .isInstanceOf[LISPJObject] && other
                   .asInstanceOf[LISPJObject]
                   .raw == this.asInstanceOf[LISPJObject].raw) {
        return true
      } else if (other.isInstanceOf[LISPSymbol] && this
                   .isInstanceOf[LISPSymbol]) {
        return (other.numberp && this.numberp && other.asNumber == this.asNumber) ||
          (other.stringp && this.stringp && other.asString == this.asString) ||
          (other.symbolp && this.symbolp && other
            .asInstanceOf[LISPSymbol]
            .raw == this.asInstanceOf[LISPSymbol].raw)
      } else if (other.isInstanceOf[LISPPair] && this.isInstanceOf[LISPPair]
                 && other.asInstanceOf[LISPPair].first ==
                   this.asInstanceOf[LISPPair].first
                 && other.asInstanceOf[LISPPair].rest ==
                   this.asInstanceOf[LISPPair].rest) {
        return true
      } else {
        return false
      }
    }

    def +(other: LISPPair): LISPObject =
      throw new EvaluationException("Incompatible types", null)
    def -(other: LISPPair): LISPObject =
      throw new EvaluationException("Incompatible types", null)
    def /(other: LISPPair): LISPObject =
      throw new EvaluationException("Incompatible types", null)
    def *(other: LISPPair): LISPObject =
      throw new EvaluationException("Incompatible types", null)

    def <(other: LISPPair): LISPObject =
      throw new EvaluationException(
        "not implemented for type combination",
        null
      )
    def >(other: LISPPair): LISPObject =
      throw new EvaluationException(
        "not implemented for type combination",
        null
      )

    override def equals(obj: Any): Boolean = {
      return obj.isInstanceOf[LISPObject] && eqp(obj.asInstanceOf[LISPObject])
    }
  }

  object LISPObject extends LISPSymbol("t")

  sealed case class LISPFunction(
      val params: Seq[Tuple2[LISPSymbol, ParamInfo]],
      val apply: Environment => LISPObject,
      val macrop: Boolean = false,
      val env: Option[Environment] = None,
      val isSymbols: LISPObject = LISPNil
  ) extends LISPObject {
    override def toString =
      "Procedure:\n" +
        s"    parameters = $params \n" +
        s"    apply      = $params \n" +
        s"    macro      = $macrop \n" +
        s"    bound env  = $env \n" +
        s"    definition = $isSymbols"
  }

  sealed case class LISPPair(var first: LISPObject, var rest: LISPObject)
      extends LISPObject {
    override def +(other: LISPPair): LISPObject =
      LISPPair(this.asList.appendedAll(other.asList))

    def asList: List[LISPObject] = asListInternal(Nil)

    @tailrec
    def asListInternal(accumulator: List[LISPObject]): List[LISPObject] = {
      if (rest.nilp) {
        return accumulator.appended(first)
      } else {
        return rest
          .asInstanceOf[LISPPair]
          .asListInternal(accumulator.appended(first))
      }
    }

    def asEnvironment(parent: Environment): Environment = {
      val list = asList
      val env  = parent.copy

      list.foreach(
        o =>
          env.putUnrestricted(
            o.asInstanceOf[LISPPair].first.asInstanceOf[LISPSymbol],
            o.asInstanceOf[LISPPair].rest.asInstanceOf[LISPObject]
          )
      )
      return env
    }

    def asSymbolList: Collection = {
      val i = new Collection(first.asToken, null)
      asSymbolList(i, i)
    }

    @tailrec
    def asSymbolList(init: Collection, last: Collection): Collection =
      if (rest.isInstanceOf[LISPPair]) {
        last.second =
          new Collection(rest.asInstanceOf[LISPPair].first.asToken, null)
        rest
          .asInstanceOf[LISPPair]
          .asSymbolList(init, last.second.asInstanceOf[Collection])
      } else {
        last.second = rest.asToken
        init
      }

    override def asToken: Token = asSymbolList

    def isCircular = isCircularIntr(Vector(), false)

    @tailrec
    def isCircularIntr(pst: Vector[LISPPair], acc: Boolean): Boolean =
      if (acc) {
        true
      } else if (this.rest.isInstanceOf[LISPPair]) {
        rest
          .asInstanceOf[LISPPair]
          .isCircularIntr(
            pst.appended(this),
            pst.find(_ eq this) != None
          )

      } else
        false

    @tailrec
    def getUniqueSliceFromCircularList(
        pst: Vector[LISPPair],
        acc: LISPObject
    ): LISPObject =
      if (pst.find(_ eq this) != None)
        acc
      else
        rest
          .asInstanceOf[LISPPair]
          .getUniqueSliceFromCircularList(
            pst.appended(this),
            acc + new LISPPair(first, LISPNil)
          )

    override def toString =
      if (!isCircular) {
        try {
          s"(${asList.mkString(" ")})"
        } catch {
          case _: Throwable => s"($first . $rest)"
        }
      } else {
        s"(${getUniqueSliceFromCircularList(Vector(), LISPNil)
          .asInstanceOf[LISPPair]
          .asList
          .mkString(" ")} ...)"
      }
  }

  sealed case class LISPSymbol(val raw: String) extends LISPObject {
    override def toString = raw
    override def asToken =
      if (this eqp LISPNil) new Collection(null, null) else super.asToken

    def na(other: LISPPair): LISPObject =
      (asNumber + other.asList.map(_.asNumber).sum).asLISPNumber

    override def +(other: LISPPair): LISPObject =
      if (this.nilp)
        LISPNil + other
      else if (this.numberp)
        na(other)
      else if (this.stringp)
        (this :: other.asList)
          .map(it => if (it.stringp) it.asString else it.toString)
          .reduce((x, y) => x + y)
          .asLISPString
      else super.+(other)

    override def /(other: LISPPair): LISPObject = {

      (asNumber :: other.asList.map(_.asNumber))
        .reduce((x, y) => x / y)
        .asLISPNumber
    }
    override def -(other: LISPPair): LISPObject = {
      (asNumber :: other.asList.map(_.asNumber))
        .reduce((x, y) => x - y)
        .asLISPNumber
    }
    override def *(other: LISPPair): LISPObject =
      new LISPSymbol(
        (asNumber :: other.asList
          .map(_.asNumber)).product.bigDecimal.toPlainString()
      )
    override def >(other: LISPPair) =
      if (other.asList
            .map(_.asNumber)
            .forall(this.asNumber > _))
        LISPObject
      else
        LISPNil
    override def <(other: LISPPair) =
      if (other.asList
            .map(_.asNumber)
            .forall(this.asNumber < _))
        LISPObject
      else
        LISPNil
  }

  final case class SyntaxTokenInfo(
      val subSymbols: Seq[(Symbol, Int, Option[Int], Int)]
  )

  object SyntaxTokenInfo {
    def buildSTIFromCollection(
        coll: Collection
    ): SyntaxTokenInfo = {
      return new SyntaxTokenInfo(
        coll.tks.zipWithIndex.map(e => getSTIDef(e._1, e._2)).flatten.toSeq
      )
    }

    private def getSTIDef(
        token: Token,
        index: Int
    ): List[(Symbol, Int, Option[Int], Int)] = {
      if (token.isInstanceOf[Symbol]) {
        return List((token.asInstanceOf[Symbol], -1, Some(0), -1))
      } else {
        val cl  = token.asInstanceOf[Collection]
        val tks = cl.tks(0)
        val tkr = cl.tks.tail
        return (tks.asInstanceOf[Symbol], -1, None, -1) :: tkr
          .map(_.asInstanceOf[Collection])
          .map(
            e =>
              (
                e.tks.head.asInstanceOf[Symbol],
                e.tks(1).raw.toInt,
                if (e.tks.size >= 3)
                  Some(e.tks(2).raw.toInt)
                else None,
                index
              )
          )
      }
    }

    def getPartabFromTokensAndSTI(
        sti: SyntaxTokenInfo,
        tokens: Seq[Token]
    ): Map[Symbol, Token] = {
      return sti.subSymbols.zipWithIndex
        .map(
          r =>
            (r._1._1, (r._1._3 match {
              case None =>
                new Collection(
                  tokens(r._1._4 match {
                    case -1     => r._2
                    case i: Int => i
                  }).asInstanceOf[Collection]
                    .tks
                    .splitAt(r._1._2)
                    ._2
                    .toList
                )
              case Some(int) =>
                if (r._1._2 == -1)
                  tokens(r._2)
                else
                  new Collection(
                    tokens(r._1._4)
                      .asInstanceOf[Collection]
                      .tks
                      .slice(r._1._2, r._1._3.get)
                  )
            }))
        )
        .toMap
    }
  }

  sealed case class LISPSyntaxObject(
      val tk: Token,
      val realtc: Int,
      val syml: SyntaxTokenInfo,
      val vararg: Boolean
  ) extends LISPObject

  object LISPSyntaxObject {
    def expand(
        so: LISPSyntaxObject,
        param: Map[Symbol, Token],
        additional: Seq[Token]
    ): Token = {
      return mappartab(so.tk, param, additional)
    }

    private def mappartab(
        tk: Token,
        partab: Map[Symbol, Token],
        additional: Seq[Token]
    ): Token = {
      if (tk.isInstanceOf[Symbol]) {
        if (!tk.raw.startsWith("^")) {
          if (tk.raw.startsWith("@")) {
            val ex3: String = tk.raw.drop(1)
            if (ex3 == "xall") {
              return new Collection(
                partab.toList.map(_._2).appendedAll(additional)
              )
            } else {
              ???
            }
          }
          if (tk.raw == "_") {
            return new Collection(additional.toList)
          }
        }
        val escflag = tk.raw.startsWith("^")
        val tk2 =
          if (tk.raw.startsWith("^")) new Symbol(tk.raw.drop(1)) else tk
        return if (escflag) tk2
        else partab.get(tk2.asInstanceOf[Symbol]).getOrElse(tk2)
      } else {
        val stpair = tk.asInstanceOf[Collection].pair
        return if (!stpair)
          new Collection(
            tk.asInstanceOf[Collection]
              .tks
              .map(
                i =>
                  if (i.raw != "_")
                    Seq(mappartab(i, partab, additional))
                  else
                    additional
              )
              .flatten
          )
        else
          new Collection(
            if (tk.asInstanceOf[Collection].first.raw == "_")
              new Collection(additional.toList)
            else
              mappartab(tk.asInstanceOf[Collection].first, partab, additional),
            if (tk.asInstanceOf[Collection].second.raw == "_")
              new Collection(additional.toList)
            else
              mappartab(tk.asInstanceOf[Collection].second, partab, additional)
          )
      }
    }
  }

  sealed case class LISPJObject(val raw: Any) extends LISPObject

  final case class ParamInfo(
      val vararg: Boolean = false,
      val optional: Boolean = false
  )

  object LISPNil extends LISPSymbol("nil") {
    override def toString()          = "nil"
    override def asToken             = new Collection(null, null)
    override def +(others: LISPPair) = LISPPair(others.asList)
  }

  object LISPPair {
    def apply(list: List[LISPObject]): LISPObject = {
      if (list.isEmpty) {
        return LISPNil
      }
      return new LISPPair(
        list.head,
        if (list.tail.isEmpty) LISPNil else apply(list.tail)
      )
    }

    def createFromTokenList(list: Collection): LISPObject = {
      try {
        if (list.nil || list.tks.isEmpty) {
          return LISPNil
        }
      } catch {
        case _: Throwable =>
      }
      if (list.pair) {
        return new LISPPair(
          if (list.first.isInstanceOf[Symbol])
            new LISPSymbol(list.first.raw)
          else
            createFromTokenList(list.first.asInstanceOf[Collection]),
          if (list.second.isInstanceOf[Symbol])
            new LISPSymbol(list.second.raw)
          else
            createFromTokenList(list.second.asInstanceOf[Collection])
        )
      }
      return new LISPPair(
        if (list.first.isInstanceOf[Symbol])
          new LISPSymbol(list.first.raw)
        else
          createFromTokenList(list.first.asInstanceOf[Collection]),
        if (list.second.isInstanceOf[Collection] && list.second
              .asInstanceOf[Collection]
              .nil)
          LISPNil
        else
          createFromTokenList(list.second.asInstanceOf[Collection])
      )
    }
  }

  implicit class StringExt(val string: String) {
    def asLISPSymbol = new LISPSymbol(string)
    def asLISPString = new LISPSymbol("\"" + string + "\"")
  }

  implicit class NumberExt(val number: BigDecimal) {
    def asLISPNumber = new LISPSymbol(number.bigDecimal.toPlainString())
  }

  implicit class ANumberExt(val number: Any) {
    def asLISPNumber = new LISPSymbol(number.toString())
  }

  implicit class AExt(val obj: Any) {
    def asLISPJObject = new LISPJObject(obj)
  }
}
