package cn.librechair.r4rs.tokenizer

import java.{util => ju}
import cn.librechair.r4rs.interpreter.types.`package`.LISPPair
import cn.librechair.r4rs.interpreter.Mrt
import cn.librechair.r4rs.interpreter.types.`package`.LISPSymbol
import cn.librechair.r4rs.interpreter.types.`package`.LISPNil

class Collection(var first: Token, var second: Token)
    extends Token(toString()) {

  def this(tks: List[Token]) =
    this(
      if (!tks.isEmpty) tks.head else null,
      if (!tks.isEmpty) new Collection(tks.tail) else null
    )

  def nil  = first == null && second == null
  def pair = !second.isInstanceOf[Collection] && second != null

  def tks: List[Token] = {
    if (first == null) {
      return Nil
    } else {
      var stacklast = this
      var lis = List[Token]()
      while (!stacklast.nil) {
        lis = lis.appended(stacklast.first)
        stacklast = stacklast.second.asInstanceOf[Collection]
      }
      return lis
    }
  }

  override def toString =
    try {
      if (pair) s"(${tks.head} . ${tks(1)})" else s"(${tks.mkString(" ")})"
    } catch {
      case _: Throwable => s"(${first.toString()} . ${second.toString()})"
    }

  override def asLisp =
    if (pair)
      new LISPPair(first.asLisp, second.asLisp)
    else
      LISPPair.createFromTokenList(this)
}
