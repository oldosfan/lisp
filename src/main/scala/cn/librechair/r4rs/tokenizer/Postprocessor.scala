/* Postprocessor.scala -- S-expression post-processor for LISPs
 * This program is free software; you are free to redistribute it
 * under the GNU General Public License, version 3, or any later ver
 * sion as published by the Free Software Foundation. Version 3.0 of
 * the GPL can be found at <https://www.gnu.org/licenses/>
 * This program includes no warranty, and no guarantee of fitness fo
 * r a particular purpose, unless otherwise specified in writing. It
 * is, however, distributed with the hope that it will be useful.
 */

package cn.librechair.r4rs.tokenizer

import scala.annotation.tailrec
import scala.collection.JavaConverters._
import java.{util => ju}
import com.github.ghik.silencer.silent
import cn.librechair.r4rs.exceptions.`package`.SyntaxException

class Postprocessor(val tokenizer: Tokenizer) {
  def poll(): Token = {
    val tokenized = tokenizer.poll()
    if (tokenized.isInstanceOf[Token]) {
      return tokenized.asInstanceOf[Token].asSymbol
    } else {
      return processPolledList(
        tokenized.asInstanceOf[List[AnyRef]]
      )
    }
  }

  private def buildDottedCollection(
      prec: List[Token],
      ovrec: Token
  ): Collection = {
    if (prec.length > 1)
      return new Collection(prec.head, buildDottedCollection(prec.tail, ovrec))
    else
      return new Collection(prec.head, ovrec)
  }

  @silent("unchecked")
  final def processPolledList(list: List[AnyRef]): Token = {
    if (list.exists(e => e.isInstanceOf[Token] &&
                         e.asInstanceOf[Token].dflag)) {
      val last = list.last
      val back = list.dropRight(2)
      if (list.indexOf(new Token(".", true)) != list.size - 2)
        throw new SyntaxException("invalid dot context")
      if (back.size < 1)
        throw new SyntaxException("no preceeding dot token")
      return buildDottedCollection(
        back.map(
          r =>
            if (r.isInstanceOf[Token]) r.asInstanceOf[Token].asSymbol
            else processPolledList(r.asInstanceOf[List[AnyRef]])
        ),
        if (last.isInstanceOf[Token]) last.asInstanceOf[Token].asSymbol
        else processPolledList(last.asInstanceOf[List[AnyRef]])
      )
    } else
      new Collection(
        list.map(
          r =>
            if (r.isInstanceOf[Token]) r.asInstanceOf[Token].asSymbol
            else processPolledList(r.asInstanceOf[List[AnyRef]])
        )
      )
  }
}
