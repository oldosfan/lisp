package cn.librechair.r4rs.tokenizer
import cn.librechair.r4rs.interpreter.types._

class Symbol(raw: String) extends Token(raw) {
  def asLISPSymbol = new LISPSymbol(raw)
}
