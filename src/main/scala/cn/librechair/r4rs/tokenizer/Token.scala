package cn.librechair.r4rs.tokenizer

import cn.librechair.r4rs.interpreter.types._

case class Token(val raw: String, val dflag: Boolean = false) {
  override def toString(): String = raw
  def asSymbol                    = new Symbol(raw)
  def asLisp: LISPObject =
    if (this.isInstanceOf[Symbol]) raw.asLISPSymbol
    else ???
}
