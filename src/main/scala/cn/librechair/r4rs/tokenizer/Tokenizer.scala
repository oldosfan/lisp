/* tokenizer.scala -- S-expression lexer
 * tokenizer.scala is free software; you are free to redistribute it
 * under the GNU General Public License, version 3, or any later ver
 * sion as published by the Free Software Foundation. Version 3.0 of
 * the GPL can be found at <https://www.gnu.org/licenses/>
 * This program includes no warranty, and no guarantee of fitness fo
 * r a particular purpose, unless otherwise specified in writing. It
 * is, however, distributed with the hope that it will be useful.
 */

package cn.librechair.r4rs.tokenizer

import cn.librechair.r4rs.TreeBuilder
import java.io.StringReader
import java.{util => ju}
import java.io.Reader
import cn.librechair.r4rs.exceptions.SyntaxException
import java.io.EOFException
import scala.jdk.CollectionConverters._

class Tokenizer(val raw: Reader) {
  import Tokenizer._

  var escape      = false
  var delim       = false
  var noCheck     = true
  var comment     = false
  var wordBuf     = new ju.LinkedList[Char]()
  var readbackBuf = Seq[Char]()
  var charCount   = 0
  val builder     = new TreeBuilder[Token]()

  def poll(): Any = {
    while (noCheck || builder.isOpened) {
      val tok = read()
      charCount += 1
      if (comment) {
        if (tok == NEWL) {
          comment = false
        }
      } else if (escape) {
        wordBuf.add(tok)
        escape = false
      } else if (tok == COMMENT) {
        comment = true
      } else if (OPEN.contains(tok) && !delim) {
        if (!builder.isOpened && !readbackBuf.isEmpty) {
          readbackBuf = readbackBuf.appended(tok)
          val wtk = new Token(wordBuf.asScala.mkString)
          wordBuf.clear()
          return wtk
        }
        if (!wordBuf.isEmpty() && builder.isOpened) {
          builder.append(new Token(wordBuf.toArray().mkString))
        }
        wordBuf.clear()
        builder.open()
        noCheck = false
      } else if (MACROS.get(tok) != None && !delim) {
        if (!wordBuf.isEmpty()) {
          noCheck = false
          if (builder.isOpened) {
            builder.append(new Token(wordBuf.toArray().mkString))
            wordBuf.clear()
          } else {
            val ret = new Token(wordBuf.toArray().mkString)
            wordBuf.clear()
            noCheck = true
            return ret
          }
        }
        noCheck = false
        builder.open()
        builder.append(new Token(MACROS.get(tok).getOrElse("")))
        val tk2 = new Tokenizer(raw)
        tk2.readbackBuf = readbackBuf
        val p   = tk2.poll()
        readbackBuf = tk2.readbackBuf
        if (p.isInstanceOf[Token]) {
          builder.append(p.asInstanceOf[Token])
        } else {
          builder.stackAppendAll(p.asInstanceOf[List[Any]])
        }
        builder.close()
      } else if (CLOSE.contains(tok) && !delim) {
        if (!builder.isOpened) {
          if (wordBuf.isEmpty()) {
            throw new SyntaxException(
              s"Unmatched closing bracket at character ${charCount}"
            )
          } else {
            val wbs = wordBuf.asScala.mkString
            wordBuf.clear()
            readbackBuf = readbackBuf.appended(tok)
            return new Token(wbs)
          }
        }
        if (!wordBuf.isEmpty()) {
          if (builder.isOpened) {
            builder.append(new Token(wordBuf.toArray().mkString))
            wordBuf.clear()
          } else {
            val ret = new Token(wordBuf.toArray().mkString)
            wordBuf.clear()
            noCheck = false
            return ret
          }
        }
        wordBuf.clear()
        builder.close()
      } else if (tok == ESC) {
        escape = true
      } else if (tok == DOT && wordBuf.isEmpty) {
        builder.append(new Token(".", true))
      } else if (tok == QUOT) {
        if (!wordBuf.isEmpty() && !delim) {
          noCheck = false
          if (builder.isOpened) {
            builder.append(new Token(wordBuf.toArray().mkString))
            wordBuf.clear()
          } else {
            val ret = new Token(wordBuf.toArray().mkString)
            wordBuf.clear()
            noCheck = true
            return ret
          }
        }
        delim = !delim
        wordBuf.add(tok)
      } else if (DELIM.contains(tok) && !delim) {
        if (!wordBuf.isEmpty()) {
          noCheck = false
          if (builder.isOpened) {
            builder.append(new Token(wordBuf.toArray().mkString))
            wordBuf.clear()
          } else {
            val ret = new Token(wordBuf.toArray().mkString)
            wordBuf.clear()
            noCheck = true
            return ret
          }
        }
      } else if (delim || !DELIM.contains(tok)) {
        wordBuf.add(tok)
      }
    }
    val ret = builder.build()
    builder.reset()
    noCheck = true
    return ret
  }

  def read(): Char = {
    if (readbackBuf.isEmpty) {
      return raw.read() match {
        case -1 =>
          if (builder.isOpened) {
            builder.reset()
            noCheck = true
            throw new SyntaxException("Unexpected end-of-file")
          } else throw new EOFException()
        case i: Any => i.asInstanceOf[Char]
      }
    } else {
      val ret = readbackBuf.last
      readbackBuf = readbackBuf.dropRight(1)
      return ret
    }
  }
}

object Tokenizer {
  val OPEN    = Seq('(', '[')
  val CLOSE   = Seq(')', ']')
  val MQUOT   = '\''
  val ESC     = '\\'
  val DELIM   = Seq(' ', '\n', '\t')
  val QUOT    = '"'
  val NEWL    = '\n'
  val REFL    = '&'
  val COMMENT = ';'
  val HASH    = '#'
  val QQ      = '`'
  val UQ      = ','
  val LC      = '~'
  val DOT     = '.'
  val MACROS = Map(
    MQUOT -> "quote",
    REFL  -> "unpack",
    HASH  -> "expand",
    QQ    -> "quasiquote",
    UQ    -> "unquote",
    LC    -> "lcons"
  )
}
